<?php
/*
 * Arquivo de configurações básicas para o funcionamento do sistema.
 */
session_start();

// Mostra todos os erros do php.
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

// Variavéis pre definidas.
define("EMP_NAME", "NAFL Lan House");
define('EMP_SLOGAN', 'A sua conexão em alta velocidade');
define('EMP_LOGO', 'img/logonafl.jpeg');
define('EMP_PG_TITLE', "NAFL Lan House :: Stoke!");
define('SMARTY',$_SERVER['DOCUMENT_ROOT'].'/stoke/vendor/smarty/libs/Smarty.class.php');
define('FRONTCONTROLLER',$_SERVER['DOCUMENT_ROOT'].'/stoke/src/classes/frontController.php');
define('LOGINCONTROLLER',$_SERVER['DOCUMENT_ROOT'].'/stoke/src/classes/loginController.php');

function __autoload($classe)
{
	$pastas = array ('classes','controller','model','view');
	foreach ($pastas as $pasta)
	{

		if(file_exists($_SERVER['DOCUMENT_ROOT'].'/stoke/src/'.$pasta.'/'.$classe.'.php'))
		{
			include $_SERVER['DOCUMENT_ROOT']."/stoke/src/".$pasta."/".$classe.".php";
		}
		var_dump($pasta);
	}
}
?>
var http_request = false;

function makeRequest(target){
	http_request = false;

	var prod_nome = document.getElementById("prod_nome").value;
	var prod_fornecedor = document.getElementById("prod_fornecedor").value;
	var prod_pCusto = document.getElementById("prod_pCusto").value;
	var prod_pVenda = document.getElementById("prod_pVenda").value;
	var prod_quant = document.getElementById("prod_quant").value;
	var estoque_minimo =document.getElementById("estoque_minimo").value;

	var url = "stoke.rs.af.cm/"+target+"?prod_nome="+prod_nome+"&prod_fornecedor="+prod_fornecedor+"&prod_pCusto="+prod_pCusto+"&prod_pVenda="+prod_pVenda+"&prod_quant="+prod_quant+"&estoque_minimo="+estoque_minimo+"&tipo=novo item";

	if(window.XMLHttpRequest){// Mozilla, Safari...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType) {
			http_request.overrideMimeType('text/xml');
		}
	} else if (window.ActiveXObject) {// I.E
		try{
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e){
			try {
				http_request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch(e){}
		}
	}

	if(!http_request){
		alert('Giving up :( Cannot creat an XMLHTTP instance');
			return false;
	}
	http_request.onreadystatechange = alertContents;
	http_request.open('GET', url, true);
	http_request.send(null);
}

function alertContents(){
	if(http_request.readyState == 4){
		if(http_request.status == 200){
			alert('Item adicionado com sucesso.');
		} else {
			alert('There was a problem with the request.');
		}
	}
}
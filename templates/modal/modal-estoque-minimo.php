 <div id="modalEstoqueMinimo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Produtos abaixo do estoque minimo.</h3>
      </div>
      <div class="modal-body">
      <table class="table table-hover">
        <thead>
          <tr>
           <th>Id</th>
              <th>Produto</th>
              <th>Em estoque</th>
              <th>Estoque minimo</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            $database->verificaEstoqueMinimo();
            while ($rows = mysql_fetch_assoc($database->result)) {
              echo "<tr>";
              echo "  <td>".$rows['prod_id']."</td>";
              echo "  <td>".$rows['prod_nome']."</td>";
              echo "  <td>".$rows['prod_quant']."</td>";
              echo "  <td>".$rows['estoque_minimo']."</td>";
              echo "</tr>";
            }

          ?>
        </tbody>
      </table>
      </div>
      <!-- <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary">Save changes</button> 
      </div> -->
  </div>   
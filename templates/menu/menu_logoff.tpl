<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="index.php">Stoke!</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li class="active"><a href="index.php">Home</a></li>
              <li><a href="#sobre" data-toggle="modal">Sobre</a></li>
              <li><a href="#contato" data-toggle="modal">Contato</a></li>
            </ul>
            <form class="navbar-form pull-right" method="POST" action="src/controller/login.php">
              <input class="span2" type="text" name="user" placeholder="Usuário">
              <input class="span2" type="password" name="password" placeholder="Password">
              <button type="submit" class="btn">Sign in</button>
            </form>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

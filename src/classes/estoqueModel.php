<?php
class estoqueModel{

	var $conex;
	var $db;
	var $result;
	var $prod_quantTotal;
	var $prod_id;
	var $data;
	var $hora;

	/*
	 * Classe construtura, define o estado inicial ao declarar a classe.
	 */
	function __construct(){
		$services_json = json_decode(getenv("VCAP_SERVICES"),true);
		$mysql_config = $services_json["mysql-5.1"][0]["credentials"];
		$username = $mysql_config["username"];
		$password = $mysql_config["password"];
		$hostname = $mysql_config["hostname"];
		$port = $mysql_config["port"];
		$sdb = $mysql_config["name"];
		$this->conex = mysql_connect("$hostname:$port", $username, $password);
		if (!$this->conex) { die("Erro ao se conecatar com o mysql.");}
		$this->db = mysql_select_db($sdb, $this->conex);
		if (!$this->db) {die("Erro ao selecionar o banco de dados.");}
	}

	function __destruct(){

	}

	/*
	 * Função para grvar log.
	 */
		public function gravaLog($event, $prod_nome, $prod_quantidade){
			$this->data = date("Y-m-d");
			$this->hora = date("h:m:s");

			$this->result = mysql_query("INSERT INTO tb_log (event_tipo, event_data, event_hora, event_quant, prod_nome)	VALUES ('$event','$this->data','$this->hora','$prod_quantidade','$prod_nome')");
			if(!$this->result){die("Erro na função: gravaLog.".mysql_error()."\n");}
		}
	/*
	 * Função que retorna todos as entradas da tabela log, para a exibição do histórico.
	 * @Param: $tipo, (entrada ou saida).
	 */
		public function getLog($tipo){
			$this->result = mysql_query("SELECT * FROM tb_log WHERE event_tipo='$tipo' ORDER BY event_id DESC LIMIT 10");
			if(!$this->result){die("Erro ao selecionar as ultimas entradas.".mysql_error());}
		}

	/*
	 * Função que retorna todos os campos da tabela produtos.
	 */
		public function getProdByName($prod_nome){
			$this->result = mysql_query("SELECT * FROM tb_produtos WHERE prod_nome='$prod_nome'",$this->conex);
			if(!$this->result){die("Erro na função: getProdByName.".mysql_error()."\n");}
		}
		public function getProdById($prod_id){
			$this->result = mysql_query("SELECT * FROM tb_produtos WHERE prod_id='$prod_id'",$this->conex);
			if(!$this->result){die("Erro na função: getProdById.".mysql_error()."\n");}			
		}
		private function getProdIdByName($prod_nome){
			$this->prod_id = mysql_query("SELECT prod_id FROM tb_produtos WHERE prod_nome='$prod_nome'",$this->conex);
			$this->prod_id = mysql_result($this->prod_id,0);
			if(!$this->prod_id){die("Erro ao selecionar a Id do produto.".mysql_error());}
		}
		public function getTodosItensDeUmaColuna($nome_coluna,$tabela){
			$this->result = mysql_query("SELECT .'$nome_coluna'. FROM .'$tabela'");
			if(!$this->result){die("Erro na função: getTodosItensDeUmaColuna.".mysql_error());}
		}
		public function getProdNomes(){
			$this->result = mysql_query("SELECT prod_nome FROM tb_produtos");
			if(!$this->result){die("Erro na função: getProdNames.".mysql_error());}
		}

	/*
	 *  Funções para entrada e saida de produtos do estoque. Ele busca a quantidade de produtos pelo nome do item.
	 */
		public function entradaProduto($prod_nome,$prod_quantidade){
			$this->prod_quantTotal = mysql_query("SELECT prod_quant FROM tb_produtos WHERE prod_nome ='$prod_nome'");
			$this->prod_quantTotal = mysql_result($this->prod_quantTotal, 0);
			if(!$this->prod_quantTotal){die("Erro ao somar a quantidade no estoque.".mysql_error()."\n");}
			$this->prod_quant = $this->prod_quantTotal + $prod_quantidade;
			$this->q = mysql_query("UPDATE tb_produtos SET prod_quant=$this->prod_quant WHERE prod_nome='$prod_nome'",$this->conex);	
			if(!$this->q){ die("Falha ao adicionar o produto no sistema. Erro na função entradaProduto().".mysql_error());}
			$this->gravaLog('entrada',$prod_nome,$prod_quantidade);
		}
		public function saidaProduto($prod_nome,$prod_quantidade){
			$this->prod_quantTotal = mysql_query("SELECT prod_quant FROM tb_produtos WHERE prod_nome ='$prod_nome'");
			$this->prod_quantTotal = mysql_result($this->prod_quantTotal, 0);
			if(!$this->prod_quantTotal){die("Erro ao somar a quantidade no estoque.".mysql_error()."\n");}
			$this->prod_quant = $prod_quantidade - $this->prod_quantTotal;
			$this->q = mysql_query("UPDATE tb_produtos SET prod_quant=$this->prod_quant WHERE prod_nome='$prod_nome'",$this->conex);	
			if(!$this->q){ die("Falha ao adicionar o produto no sistema. Erro na função saidaProduto().".mysql_error());}
			$this->gravaLog('saida',$prod_nome,$prod_quantidade);
		}
	/*
	 * Funão para adicionar um novo produto no banco de dados
	 */
		public function novoItem($prod_nome,$prod_fornecedor,$prod_pCusto, $prod_pVenda,$prod_quant,$estoque_minimo){
			$this->result = mysql_query("INSERT INTO tb_produtos (prod_nome,prod_fornecedor,prod_pCusto,prod_pVenda,prod_quant,estoque_minimo)	
										 VALUES ('$prod_nome','$prod_fornecedor',$prod_pCusto, $prod_pVenda,$prod_quant, $estoque_minimo)");
		 	if(!$this->result){die("Erro ao adicionar produto no banco de dados.".mysql_error());}
		 	$this->gravaLog('novo item',$prod_nome,0);
		}
	/*
	 * Função que verifica o estoque minimo de todos os produtos, e retorna se há algum que esta abaixo do estoque minimo.
	 */
		public function verificaEstoqueMinimo(){
			$this->result = mysql_query("SELECT * FROM tb_produtos WHERE prod_quant < estoque_minimo");
			if(!$this->result){die("Erro no verificamento do estoque minimo.".mysql_error());}
		}
	/*
	 * Funções para manipulação de login do usuário.
	 */
		public function logarUsuario($user, $password){
			$this->result = mysql_query("SELECT * FROM tb_users WHERE user='$user' AND password='$password'");
			if(!$this->result){die("Erro ao logar".mysql_error());}
		}
	}
?>
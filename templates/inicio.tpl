<?php
include "src/configs.php";
$database = new estoqueModel();
$login = unserialize($_SESSION['loginClasse']);
//$login = unserialize($cache->get('loginController'));
//$login->lockPage('1','index.php','inicio.php?alert=2');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo EMP_PG_TITLE; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="templates/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="../assets/ico/favicon.png">
  </head>

  <body>
 	{include file="menu/menu_logoff.tpl"}
    <div class="container">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
        <h1>Olá, <?php echo EMP_NAME; ?>!</h1>
        <p>Stoke! Seu estoque. Seu controle. Esteja atento a todas as notificações, e as situações de seu estoque para fazer novos pedidos. Estamos aqui para ajuda-lo a gerir o seu estoque de uma forma eficiente e prática.</p>
        <p><a class="btn btn-primary btn-large">Aprenda mais &raquo;</a></p>
        <!-- ALERTA DE ESTOQUE MINIMO -->
        {$alerta_estoque_minimo}
      </div>

      <!-- Example row of columns -->
      <div class="row">
        <div class="span4">
          <h2>Situação do estoque</h2>
          <p>Quantos produtos eu tenho no estoque? Qual é o valor em dinheiro do meu estoque? ATravés desta seção você terá todas estas, e mais informações.</p>
          <p><a class="btn" href="#">View details &raquo;</a></p>
        </div>
        <div class="span4">
          <h2>Entrada no estoque</h2>
          <p>Chegaram novos produtos ao seu estoque? Adicione eles aqui!</p>
          <p><a href="#modalEntradas" role="button" class="btn" data-toggle="modal">Adcionar Produtos.</a></p>
       </div>
        <div class="span4">
          <h2>Saída do estoque</h2>
          <p>Fechando o estoque, você vendeu produtos? Que ótimo, mas não se esqueça de remove-los do seu Stk!</p>
          <p><a href="#modalSaidas" role="button" class="btn" data-toggle="modal">Remover Produtos.</a></p>
        </div>
      </div>

      <hr>

      <footer>
        <p>&copy; Chama 2012</p>
      </footer>

    </div> <!-- /container -->


    <!-- MODAL ENTRADA DE PRODUTOS. -->
    {include file="modal/form-entradas.tpl"}
    <!-- MODAL PARA SAIDAS DE PRODUTOS -->
    {include file="modal/form-saidas.tpl"}
    <!-- MODAL SOBRE -->
    {include file="modal/sobre.tpl"}
    <!-- MODAL CONTATO -->
    {include file="modal/contato.tpl"}
    <!-- MODAL ADICIONA ITEM -->
    {include file="modal/form-novo-item.tpl"}
    <!-- MODAL ESTOQUE MINIO #modalEstoqueMinimo -->
    {include file="modal/modal-estoque-minimo.tpl"}

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="templates/js/jquery.js"></script>
    <script src="templates/js/bootstrap-transition.js"></script>
    <script src="templates/js/bootstrap-alert.js"></script>
    <script src="templates/js/bootstrap-modal.js"></script>
    <script src="templates/js/bootstrap-dropdown.js"></script>
    <script src="templates/js/bootstrap-scrollspy.js"></script>
    <script src="templates/js/bootstrap-tab.js"></script>
    <script src="templates/js/bootstrap-tooltip.js"></script>
    <script src="templates/js/bootstrap-popover.js"></script>
    <script src="templates/js/bootstrap-button.js"></script>
    <script src="templates/js/bootstrap-collapse.js"></script>
    <script src="templates/js/bootstrap-carousel.js"></script>
    <script src="templates/js/bootstrap-typeahead.js"></script>
    <script src="templates/js/ganalytics.js"></script>

  </body>
</html>

<!DOCTYPE html>
<?php
//include "src/configs.php";
//$database = new estoqueModel();
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>{$nome_empresa} :: Stoke!</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->
     <link href="templates/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 20px;
        padding-bottom: 40px;
      }

      /* Custom container */
      .container-narrow {
        margin: 0 auto;
        max-width: 700px;
      }
      .container-narrow > hr {
        margin: 30px 0;
      }

      /* Main marketing message and sign up button */
      .jumbotron {
        margin: 60px 0;
        text-align: center;
      }
      .jumbotron h1 {
        font-size: 72px;
        line-height: 1;
      }
      .jumbotron .btn {
        font-size: 21px;
        padding: 14px 24px;
      }

      /* Supporting marketing content */
      .marketing {
        margin: 60px 0;
      }
      .marketing p + h4 {
        margin-top: 28px;
      }
    </style>
    <link href="templates/css/bootstrap-responsive.css" rel="stylesheet">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="../assets/ico/favicon.png">
  
  </head>

  <body>

	{include file="menu/menu_logoff.tpl"}
    <div class="container-narrow">
Chegou produtos? Adicione eles ao seu Stoke!
      <div class="jumbotron">
        <h1>Seja bem vindo ao Stoke! Seu estoque, seu controle!</h1>
        <p class="lead">O Stoke é um software multiplataforma na nuvem para gerenciamento de estoques, com ele, você possui todos os dados que precisar. </p>
      </div>
      <hr>
      <div class="row-fluid marketing">
        <div class="span6">
          <h4>Controle</h4>
          <p>Donec id elit non mi porta gravida at eget metus. Maecenas faucibus mollis interdum.</p>

          <h4>Estatísticas</h4>
          <p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras mattis consectetur purus sit amet fermentum.</p>

          <h4>Subheading</h4>
          <p>Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
        </div>

        <div class="span6">
          <h4>Subheading</h4>
          <p>Donec id elit non mi porta gravida at eget metus. Maecenas faucibus mollis interdum.</p>

          <h4>Subheading</h4>
          <p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras mattis consectetur purus sit amet fermentum.</p>

          <h4>Subheading</h4>
          <p>Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
        </div>
      <hr>

      <div class="footer">
        <p>&copy; Company 2012</p>
      </div>
    <!-- MODAL SOBRE -->
    {include file="modal/sobre.tpl"}
    <!-- MODAL CONTATO -->
    {include file="modal/contato.tpl"}

    </div> <!-- /container -->
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="templates/js/jquery.js"></script>
    <script src="templates/js/jquery.validate.js"></script> <!-- Plugin jQuery Validate. -->
    <script src="templates/js/bootstrap-transition.js"></script>
    <script src="templates/js/bootstrap-alert.js"></script>
    <script src="templates/js/bootstrap-modal.js"></script>
    <script src="templates/js/bootstrap-dropdown.js"></script>
    <script src="templates/js/bootstrap-scrollspy.js"></script>
    <script src="templates/js/bootstrap-tab.js"></script>
    <script src="templates/js/bootstrap-tooltip.js"></script>
    <script src="templates/js/bootstrap-popover.js"></script>
    <script src="templates/js/bootstrap-button.js"></script>
    <script src="templates/js/bootstrap-collapse.js"></script>
    <script src="templates/js/bootstrap-carousel.js"></script>
    <script src="templates/js/bootstrap-typeahead.js"></script>
  </body>
</html>

$document.ready(function(){
	$("#form_entrada").validate({
		rules:{
			prod_nome:{
				required:true, minlenght:2
			},
			prod_quantidade:{
				required:true, minlenght:1
			},
			// Define as mensagens de erro para os seguintes campos:
			messages:{
				prod_nome:{
					required:"É obrigatório selecionar o produto.",
					minlenght: "Nome inválido"
				}
			}
		}
	});
});
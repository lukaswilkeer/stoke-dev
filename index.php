<?php
include $_SERVER['DOCUMENT_ROOT'].'/stoke/src/configs.php';
include SMARTY;

$page = $_GET['url'];

$frontController = new frontController();
if(!$page) {
  $frontController->getView('index');
} else {
  $frontController->getView($page);
}
?>

<?php
class loginController extends loginModel{

	var $user;
	var $user_privilegies = 0;

	function __construct(){
		$db = new loginModel();
		$this->user_privilegies = 0;

	}

	function __destruct(){

	}
	/*
	 * Função para logar o usuário no sistema.
	 * @access public
	 */
		public function login($user, $password){
			parent::logarUsuario($user,$password);
			if(!parent::$this->result){
				echo "Nenhum usuário selecionado.";
				$this->user_privilegies = 0;
			} else {
				$row = mysql_fetch_assoc(parent::$this->result);
				$this->user = $row['user'];
				$this->user_privilegies = $row['user_privilegies'];
				echo "Usuário logado com sucesso.";
				header("Location:http://".$_SERVER['SERVER_NAME']."/index.php?url=inicio");
				exit;
			}
		}
	/*
	 * Função para bloquear página de acesso de usuários não cadastrados ou usuários com permissões abaixo do necessário.
	 * Privilégio: 0 = COnvidado. 1 = funcionário. 2 = dono ou admin.
	 */
		public function lockPage($current_page_privillegies,$redirect_page_if_logoff,$redirect_page_if_low_privilegies){
			if (!$this->user) {
				echo "<script>alert('Você não está logado.');</script>";
				header("Location:".$redirect_page_if_logoff);
				exit;
			} else if ($this->user_privilegies < $current_page_privillegies) {
				echo "<script>alert('Você não tem permissão para acessar esta página.');</script>";
				echo "<a href='index.php'>Clique aqui para ser redirecionado.</a>";
				header("Location:".$redirect_page_if_low_privilegies);
				exit;
			} 
		}

		public function lockAcessToIndexIfLogon(){
			if($this->user_privilegies > 0){
				header("Location:inicio.php");
				exit;
			}
		}

}
?>
